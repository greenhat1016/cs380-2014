=====================================================================
CS380 GPU and GPGPU Programming
KAUST, Spring Semester 2014
Programming Assignment #3
Image Processing with GLSL and CUDA

Contacts: 
peter.rautek@kaust.edu.sa
ronell.sicat@kaust.edu.sa
=====================================================================

Tasks:

1. texture mapping
- read a sample texture from the folder "data/", use
  the bmpreader or any library you like. you
  can also convert the texture to a different format before
  you read it. 
- render the box with different textures assigned to it

2. image processing with GLSL

- implement simple operations to adjust brightness, contrast, and saturation of the texture, as described in chapter 19.5 in the GLSL book
- implement smoothing, edge detection, and sharpening as described in chapter 19.7 in the GLSL book.

IMPLEMENTATION HINTS:
basically there are two options to do this (choose one):
2.a) Frame buffer object (FBO) with two rendering passes
	pass 1: render the processed image into a target texture attached
        to an OpenGL FBO. the target texture is different from the source texture!
        perform the image processing operation(s) in this rendering pass.
	pass 2: use the resulting texture (from pass 1) and apply it as texture when rendering a mesh
2.b) Comoute shader: Use a compoute shader to process the texture and use the resulting texture when rendering a mesh

3. image processing with CUDA

- implement the same image processing operations (as in point 2: brightness, contrast, saturation, smoothing, edge detection, sharpening), but this time using CUDA

Use one of the two variants:
3.a) Simple c-style array as input for CUDA:
   use a simple c-style array as input for your CUDA kernels
   use cudaMalloc to allocate and cudaMemcpy to initialize
   device memory. Do not confuse this with the cudaArray type.

3.b) Texture as input for CUDA: 
   use a texture as input for your CUDA kernels.
   For this, you need the above mentioned cudaArray type.

4. Submit your program and a report including results for the different image processing operations comparing the GLSL and the CUDA version.
   

All settings must be accessible from your program without the need to modify the source code. 
Provide key mappings to switch between the image processing filters and operations and GLSL vs. CUDA

The provided textures are from http://www.grsites.com/
You may add other textures as well