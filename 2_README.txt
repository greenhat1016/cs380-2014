=====================================================================
CS380 GPU and GPGPU Programming
KAUST, Spring Semester 2014
Programming Assignment #2
Phong Shading and Procedural Texturing

Contacts: 
peter.rautek@kaust.edu.sa
ronell.sicat@kaust.edu.sa
=====================================================================

Tasks:

1. Setup a glsl program (at least vertex and fragment shaders). 

2. Implement Phong lighting+Gouraud shading [1] (the Phong lighting model is evaluated in the vertex shader).

3. Implement Phong lighting+Phong shading [2] (the Phong lighting model is evaluated in the fragment shader, not in the vertex shader as for Gouraud shading).

4. Set up a camera and transformations that can be manipulated using the mouse and keyboard.

5. Perform different kinds of procedural shading (in the fragment shader):
Implement the procedural shaders described in the following chapters of the "OpenGL Shading Language" book:
- 11.1 Stripes
- 11.3 Lattice

6. Provide key mappings to allow the user to switch between different kinds of shading and to set parameters for the lighting model.

7. Submit your program and a report including the comparison of Phong and Gouraud shading and results of the different procedural shading methods.

BONUS: implement (procedural) bump mapping (normal mapping) as in chapter 11.4 of the "OpenGL Shading Language" book.

See:
[1] http://en.wikipedia.org/wiki/Gouraud_shading
[2] http://en.wikipedia.org/wiki/Phong_shading