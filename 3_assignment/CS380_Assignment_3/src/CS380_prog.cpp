// CS 380 - GPGPU Programming
// KAUST, Spring Semester 2014
//
// Programming Assignment #3

// system includes
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string.h>
#include <math.h>
#include <algorithm> 

// library includes
// include glm: http://glm.g-truc.net/
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform2.hpp>

// include GLEW: http://glew.sourceforge.net/
#define GLEW_STATIC // including glew as static library
#include <GL/glew.h> 

// include glfw library: http://www.glfw.org/
#define GLFW_INCLUDE_GLCOREARB // defines that opengl core profile is used 
#include <GLFW/glfw3.h>

// includes for CUDA
#include <cuda_runtime.h>

// framework includes
#include "glutils.h"
#include "vbocube.h"
#include "vbomesh.h"
#include "glslprogram.h"
#include "vbosphere.h"

// for loading bmp files as textures
#include "bmpreader.h"

// mesh option constants
static const unsigned int MESH_CUBE = 0;
static const unsigned int MESH_SPHERE = 1;
static const unsigned int MESH_OBJ = 2;

// switch between meshes
unsigned int  m_uMeshOption = MESH_CUBE;


// active shader
int m_iShaderOption;


// pointer to the window that will be created by glfw
GLFWwindow* m_pWindow;

// window size
unsigned int m_uWindowWidth = 800;
unsigned int m_uWindowHeight = 600;

float m_fCameraZ = 30.f;

// a simple sphere
VBOSphere *m_pSphere;

// a simple cube
VBOCube *m_pCube;

// a more complex mesh
VBOMesh *m_pMesh; 

// glsl program
GLSLProgram *m_glslProgram;

// matrices for setting up camera
mat4 m_matModel;
mat4 m_matView;
mat4 m_matProjection;

// current rotation angle around the x axis
float m_fRotationAngleX;
// current rotation angle around the z axis
float m_fRotationAngleZ;
// current cursor position x 
double m_dCursorX;
// current cursor position y
double m_dCursorY;


// glfw errors are reported to this callback
static void errorCallback(int error, const char* description)
{
	fputs(description, stderr);
}

static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, GL_TRUE);
	} else if( key == GLFW_KEY_1 && action == GLFW_PRESS ) {

		m_iShaderOption = 1;
		printf( "Switched to Phong+Gouraud.\n" );
		
	} else if( key == GLFW_KEY_2 && action == GLFW_PRESS ) {

		m_iShaderOption = 2;
		printf( "Switched to Phong+Phong.\n" );
	} else if( key == GLFW_KEY_3 && action == GLFW_PRESS ) {

		m_iShaderOption = 3;
		printf( "Switched to Phong+Phong, Stripes.\n" );
	} else if( key == GLFW_KEY_4 && action == GLFW_PRESS ) {
		m_iShaderOption = 4;
		printf( "Switched to Phong+Phong, Lattice.\n" );
	} else if( key == GLFW_KEY_5 && action == GLFW_PRESS ) {
		m_iShaderOption = 5;
		printf( "Switched to texture mapping.\n" );
	} else if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS) {
		m_uMeshOption++;
		if (m_uMeshOption>2)
		{
			m_uMeshOption = 0;
		}
	} else if (key == GLFW_KEY_LEFT && action == GLFW_PRESS) {
		if (m_uMeshOption==0)
		{
			m_uMeshOption = 2;
		} else {
			m_uMeshOption--;
		}
		
	}

}


static void scrollCallback(GLFWwindow* window, double dx, double dy)
{
	m_fCameraZ += (float)dy * 0.1f;
	m_fCameraZ = std::min(1000.f, std::max(3.f, m_fCameraZ));

	m_matView = glm::lookAt(vec3(0.0f,0.0f,m_fCameraZ), vec3(0.0f,0.0f,0.0f), vec3(0.0f,1.0f,0.0f));
}

static void cursorPosCallback(GLFWwindow* window, double x, double y)
{
	if( glfwGetMouseButton( window, GLFW_MOUSE_BUTTON_1 ) == GLFW_PRESS ) {
		
		m_fRotationAngleX += x - m_dCursorX;
		if(m_fRotationAngleX >= 360.0f ) {
		//	rotAngleX = 0.0f;
		}
		m_dCursorX = x;

		m_fRotationAngleZ += y - m_dCursorY;
		if(m_fRotationAngleZ >= 360.0f ) {
		//	rotAngleY = 0.0f;
		}
		m_dCursorY = y;
	} 

	if( glfwGetMouseButton( window, GLFW_MOUSE_BUTTON_2 ) == GLFW_PRESS ) {
		scrollCallback(window, 0, y - m_dCursorY);
	}

}

static void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
{
	if( action == GLFW_PRESS ) {
		double x, y;
		glfwGetCursorPos(window, &x, &y);
		m_dCursorX = (float) x;
		m_dCursorY = (float) y;
	}
	
	
}

void windowResizeCallback(GLFWwindow* window, int width, int height)
{
	m_uWindowWidth = width;
	m_uWindowHeight = height;
	m_matProjection = glm::perspective(70.0f, (float)m_uWindowWidth/(float)m_uWindowHeight, 1.0f, 1000.0f);
	glViewport(0, 0, m_uWindowWidth, m_uWindowHeight);
}


// render a frame
void render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// use your program
	m_glslProgram->use();

	// set uniforms
	m_matModel *= glm::rotate(m_fRotationAngleX, vec3(1.0f,0.0f,0.0f));
	m_matModel *= glm::rotate(m_fRotationAngleZ, vec3(0.0f,0.0f,1.0f));

	m_fRotationAngleX *= 0.1;
	m_fRotationAngleZ *= 0.1;

	if(abs(m_fRotationAngleX) < 0.05)
	{
		m_fRotationAngleX = 0;
	}

	if(abs(m_fRotationAngleZ) < 0.05)
	{
		m_fRotationAngleZ = 0;
	}

	
	mat4 mv = m_matView * m_matModel;
	m_glslProgram->setUniform( "ModelViewMatrix", mv );
	m_glslProgram->setUniform( "NormalMatrix", mat3( vec3(mv[0]), vec3(mv[1]), vec3(mv[2]) ) );
	m_glslProgram->setUniform( "MVP", m_matProjection * mv );

	vec4 worldLight = vec4(5.0f,5.0f,2.0f,1.0f);
	m_glslProgram->setUniform( "Material.Kd", 0.9f, 0.5f, 0.3f);
	m_glslProgram->setUniform( "Light.Ld", 1.0f, 1.0f, 1.0f);
	m_glslProgram->setUniform( "Light.Position", m_matView * worldLight );
	m_glslProgram->setUniform( "Material.Ka", 0.9f, 0.5f, 0.3f);
	m_glslProgram->setUniform( "Light.La", 0.4f, 0.4f, 0.4f);
	m_glslProgram->setUniform( "Material.Ks", 0.8f, 0.8f, 0.8f);
	m_glslProgram->setUniform( "Light.Ls", 1.0f, 1.0f, 1.0f);
	m_glslProgram->setUniform( "Material.Shininess", 100.0f);

	m_glslProgram->setUniform( "ShaderType", m_iShaderOption );
	m_glslProgram->setUniform( "VarLightIntensity", vec3( 0.5f, 0.5f, 0.5f ));

	m_glslProgram->setUniform( "StripeColor", vec3( 0.5f, 0.5f, 0.0f ) );
	m_glslProgram->setUniform( "BackColor", vec3( 0.0f, 0.5f, 0.5f ) );
	m_glslProgram->setUniform( "Width", 0.5f );
	m_glslProgram->setUniform( "Fuzz", 0.1f );
	m_glslProgram->setUniform( "Scale", 10.0f );
	m_glslProgram->setUniform( "Threshold", vec2( 0.13f, 0.13f ) );
	
	switch (m_uMeshOption)
	{
		case MESH_CUBE: m_pCube->render(); break;
		case MESH_SPHERE: m_pSphere->render(); break;
		case MESH_OBJ: m_pMesh->render(); break;
	}

}

// query GPU functionality we need for OpenGL, return false when not available
bool queryGPUCapabilitiesOpenGL()
{
	// OPTIONAL: Check for required OpenGL functionality
	return true;
}

// query GPU functionality we need for CUDA, return false when not available
bool queryGPUCapabilitiesCUDA()
{
	// OPTIONAL: Check for required CUDA functionality
	return true;
}


// check for opengl error and report if any
void checkForOpenGLError()
{
	GLenum error = glGetError();
	if (error==GL_NO_ERROR) {
		return;
	}
	std::string errorString = "unknown";
	std::string errorDescription = "undefined";

	if (error==GL_INVALID_ENUM)
	{
		errorString = "GL_INVALID_ENUM";
		errorDescription = "An unacceptable value is specified for an enumerated argument. The offending command is ignored and has no other side effect than to set the error flag.";
	}
	else if (error==GL_INVALID_VALUE)
	{
		errorString = "GL_INVALID_VALUE";
		errorDescription = "A numeric argument is out of range. The offending command is ignored and has no other side effect than to set the error flag.";
	}
	else if (error==GL_INVALID_OPERATION)
	{
		errorString = "GL_INVALID_OPERATION";
		errorDescription = "The specified operation is not allowed in the current state. The offending command is ignored and has no other side effect than to set the error flag.";
	}
	else if (error==GL_INVALID_FRAMEBUFFER_OPERATION)
	{
		errorString = "GL_INVALID_FRAMEBUFFER_OPERATION";
		errorDescription = "The framebuffer object is not complete. The offending command is ignored and has no other side effect than to set the error flag.";
	}
	else if (error==GL_OUT_OF_MEMORY)
	{
		errorString = "GL_OUT_OF_MEMORY";
		errorDescription = "There is not enough memory left to execute the command. The state of the GL is undefined, except for the state of the error flags, after this error is recorded.";
	}
	else if (error==GL_STACK_UNDERFLOW)
	{
		errorString = "GL_STACK_UNDERFLOW";
		errorDescription = "An attempt has been made to perform an operation that would cause an internal stack to underflow.";
	}
	else if (error==GL_STACK_OVERFLOW)
	{
		errorString = "GL_STACK_OVERFLOW";
		errorDescription = "An attempt has been made to perform an operation that would cause an internal stack to overflow.";
	}

	fprintf(stdout, "Error %s: %s\n", errorString.c_str(), errorDescription.c_str());
}

				  
static void debugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, GLvoid* userParam);

// init glfw and opengl 
bool initGL(int argc, char **argv)
{
	glfwSetErrorCallback(errorCallback);

	// initialize the glfw library
	if (!glfwInit())
		return false;

	// set window hints
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
	
	m_pWindow = glfwCreateWindow(m_uWindowWidth, m_uWindowHeight, "CS380 - OpenGL", NULL, NULL); // Windowed
	// window = glfwCreateWindow(800, 600, "CS380 - OpenGL", glfwGetPrimaryMonitor(), nullptr); // Fullscreen

	if (!m_pWindow)
	{
		glfwTerminate();
		return false;
	}

	// make the window's context current 
	glfwMakeContextCurrent(m_pWindow);

	glfwSetKeyCallback(m_pWindow, keyCallback);
	glfwSetCursorPosCallback(m_pWindow, cursorPosCallback);
	glfwSetMouseButtonCallback(m_pWindow, mouseButtonCallback);
	glfwSetScrollCallback(m_pWindow, scrollCallback);
	glfwSetWindowSizeCallback(m_pWindow, windowResizeCallback);
	
	checkForOpenGLError();

	// init glew 
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		// glewInit failed, something is seriously wrong
		fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
		return false;
	}
	fprintf(stdout, "Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));

	// there is a bug with glew 1.10 (as reported at http://www.opengl.org/wiki/OpenGL_Loading_Library): init gives a GL_INVALID_ENUM error when using the opengl core profile. 
	// nothing to worry about when glewExperimental is set to GL_TRUE
	// however, we check for OpenGL errors here and simply ignore this one.
	GLenum error = glGetError();
	
	return true;
}
 
// draws and handles input events until window is closed
void programMainLoop()
{
	// one time initializations
	// set clear color 
	glClearColor(0.0, 0.0, 0.5, 1.0);
	
	// loop until the user closes the window
	while (!glfwWindowShouldClose(m_pWindow))
	{
		// render a frame
		render();

		// check for opengl errors
		checkForOpenGLError();
		
		// display: swap front and back buffers 
		glfwSwapBuffers(m_pWindow);

		// poll for events
		glfwPollEvents();
	}

	glfwTerminate();
}

void setupScene()
{

	glEnable(GL_DEPTH_TEST);
	m_iShaderOption = 1;

	// Set up a camera. Hint: The glm library is your friend
	m_matModel = mat4(1.0f);
	m_fRotationAngleX = 0.0f;
	m_fRotationAngleZ = 0.0f;

	m_matView = glm::lookAt(vec3(0.0f,0.0f,m_fCameraZ), vec3(0.0f,0.0f,0.0f), vec3(0.0f,1.0f,0.0f));
	m_matProjection = glm::perspective(70.0f, (float)m_uWindowWidth/(float)m_uWindowHeight, 1.0f, 1000.0f);
	vec4 worldLight = vec4(50.0f,50.0f,20.0f,1.0f);

	// Set up glsl program (at least vertex and fragment shaders). Hint: The GLSLProgram class is your friend!
	m_glslProgram = new GLSLProgram();
	m_glslProgram->compileShader( "src/vertexshader.vert" );
	m_glslProgram->compileShader( "src/fragmentshader.frag" );
	m_glslProgram->link();

	m_pCube = new VBOCube();

	m_pSphere = new VBOSphere(1, 10, 10);

	// you can try loading other meshes
	//m_pMesh = new VBOMesh("data/bottle/3d_bottle_02.obj",true,true,true);
	m_pMesh = new VBOMesh("data/bs_ears.obj",true,true,true);
	//m_pMesh = new VBOMesh("data/cart.obj",true,false,true);
	
	// load texture
	// TODO
}


void printUsage(){
	fprintf(stdout, "================================ usage ================================\n");
	fprintf(stdout, "right/left arrow: switch model\n");
	fprintf(stdout, "left mouse button to rotate\n");
	fprintf(stdout, "right mouse button or mouse wheel: zoom\n");
	fprintf(stdout, "1,2,3,4,5: switch shading mode\n");
	fprintf(stdout, "1: Phong+Gouraud.\n" );
	fprintf(stdout, "2: Phong+Phong.\n" );
	fprintf(stdout, "3: Phong+Phong, Stripes.\n" );
	fprintf(stdout, "4: Phong+Phong, Lattice.\n" );
	fprintf(stdout, "5: Texture mapping, TODO.\n" );
	fprintf(stdout, "=======================================================================\n");
}


// entry point
int main(int argc, char** argv)
{
	// init opengl
	if (!initGL(argc, argv)) 
	{
		// quit in case initialization fails
		return -1;
	}

	// query opengl capabilities
	if (!queryGPUCapabilitiesOpenGL()) 
	{
		// quit in case capabilities are insufficient
		return -1;
	}

	// query cuda capabilities
	if(!queryGPUCapabilitiesCUDA())
	{
		// quit in case capabilities are insufficient
		return -1;
	}
	
	// one time setup
	setupScene();

	// print how to use the program:
	printUsage();

	// start traversing the main loop
	programMainLoop();
}

